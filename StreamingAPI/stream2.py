# -*- coding: utf-8 -*-
##### BEGIN IMPORT #####
import json
from pymongo import MongoClient

from twitter import Twitter, OAuth, TwitterHTTPError, TwitterStream
try:
    import json
except ImportError:
    import simplejson as json

##### END IMPORT #####

##### BEGIN AUTHENTICATION #####

access_token = '178121845-hpSZn2MiHsghf8SQSUXlWrvibIP8vrq34Js8iQHQ'
access_secret = '6CFpYyYdwxTmLqWM8y4qdr6mVgUV6g28C4tYbQz0qkqpd'
consumer_key = 'ni1Iem3m6JMad29CB70tNIkhH'
consumer_secret = 'qsZ55TMxgpvPjmjOIu1iLGWWnct4CgUpHjvrJsm3NYEkMij9cS'

oauth = OAuth(access_token, access_secret, consumer_key, consumer_secret)

# conect to STREAM API
twitter_stream = TwitterStream(auth = oauth)

##### END AUTHENTICATION #####

##### BEGIN STAGE ONE #####

# get tweets containing "YOUR_QUERY" and save them to a file
# https://dev.twitter.com/streaming/reference/post/statuses/filter
WORDS = ['#RodaViva','Moro','#TVCultura']
tweets = twitter_stream.statuses.filter(track = WORDS)
with open('tweets-search.txt', 'w') as b:
    for tws in tweets:
        b.write(json.dumps(tws, indent = 4, sort_keys = True))
        b.write('\n')

##### END STAGE ONE #####

##### BEGIN STAGE TWO #####

# get tweets containing "YOUR_QUERY" and save them to a file (JSON response manipulation)
# https://dev.twitter.com/streaming/reference/post/statuses/filter
tweet = twitter_stream.statuses.filter(track = "YOUR_QUERY")
with open('tweets-search-JSON response manipulated.txt', 'w') as c:
    for tws in tweet:
        c.write(str('@' + tws['user']['screen_name'] + '\n'))
        c.write(str(tws['text'].encode("utf-8") + '\n\n'))

##### END STAGE TWO #####