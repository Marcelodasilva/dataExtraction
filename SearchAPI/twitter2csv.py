
# coding: utf-8

# In[104]:


#import pandas as pd

#import requests
#import json
#import re
#import pprint
#from pymongo import MongoClient

#import sys
#import os
#import jsonpickle
#import tweepy
import time
import string
import csv
import twitter


# In[105]:


TWITTER_API_KEY = 'ni1Iem3m6JMad29CB70tNIkhH'
TWITTER_API_SECRET = 'qsZ55TMxgpvPjmjOIu1iLGWWnct4CgUpHjvrJsm3NYEkMij9cS'
TWITTER_ACCESS_TOKEN = '178121845-hpSZn2MiHsghf8SQSUXlWrvibIP8vrq34Js8iQHQ'
TWITTER_ACCESS_TOKEN_SECRET = '6CFpYyYdwxTmLqWM8y4qdr6mVgUV6g28C4tYbQz0qkqpd'

#FPP_KEY = 'fXZLbyUbnR-sk8B0DFkc7AZCuLvgxuvv'
#FPP_SECRET = '3OqwElEu2RtmfsqWZdylujm6qqEQ8RL3'

#MONGO_HOST= 'mongodb://localhost/twitterdb'

def authApi():
    """
    credenciais para autenticação
    """
    api = twitter.Api(consumer_key=TWITTER_API_KEY,
                      consumer_secret=TWITTER_API_SECRET,
                      access_token_key=TWITTER_ACCESS_TOKEN,
                      access_token_secret=TWITTER_ACCESS_TOKEN_SECRET)

    return api


# In[106]:


def format_filename(s):
    """Take a string and return a valid filename constructed from the string.
Uses a whitelist approach: any characters not present in valid_chars are
removed. Also spaces are replaced with underscores.
 
Note: this method may produce invalid filenames such as ``, `.` or `..`
When I use this method I prepend a date string like '2009_01_15_19_46_32_'
and append a file extension like '.txt', so I avoid the potential of using
an invalid filename.
 
"""
    valid_chars = "-_.() {}{}".format(string.ascii_letters, string.digits)
    filename = ''.join(c for c in s if c in valid_chars)
    filename = filename.replace(' ','_')
    return filename

def twitter_search(query, max_tweets,delimiter = ';'):
    api = authApi()
    searchQuery = query  # termo buscado
    maxTweets = max_tweets  # variável com maximo de tweets a serem obtidos
    tweetsPerQry = 100  # limite máximo de tweets por requisição

    #allTweets = []  # lista final a ser retornada com todos os tweets
    # geocode = ("-2.507389", "-54.952306", "5km")  # ooordenada e raio de abrangência

    sinceId = None
    max_id = -1
    tweetCount = 0
    
    timestr = time.strftime("%Y%m%d-%H%M%S")
    searchQuery += searchQuery + '-filter:retweets'
#     separator = 'separator=,'
    print("baixando no maximo {0} tweets".format(maxTweets))
    print('executando a seguinte query \n {}\n'.format(query))
    
    filename = 'results_query_{}_timestamp_{}'.format(query,timestr)
    fullfilename = '{}.csv'.format(format_filename(filename)) 
    with open(fullfilename, 'w',newline='',encoding='utf-8') as f:
        spamwriter = csv.writer(f, delimiter=delimiter,quotechar='"', quoting=csv.QUOTE_ALL)
#         spamwriter.writerow([separator])
        spamwriter.writerow(['created_at','favorite_count','retweet_count','text','hashtags','urls'])
        while tweetCount < maxTweets:
            if (max_id <= 0):
                if (not sinceId):
                    new_tweets = api.GetSearch(term=searchQuery, count=tweetsPerQry)
                else:
                    new_tweets = api.GetSearch(term=searchQuery, count=tweetsPerQry,
                                               since_id=sinceId)
            else:
                if (not sinceId):
                    new_tweets = api.GetSearch(term=searchQuery, count=tweetsPerQry,
                                               max_id=str(max_id - 1))
                else:
                    new_tweets = api.GetSearch(term=searchQuery, count=tweetsPerQry,
                                               max_id=str(max_id - 1),
                                               since_id=sinceId)
            if not new_tweets:
                print("sem mais tweets encontrados")
                break
            tweetCount += len(new_tweets)
            for tweet in new_tweets:
#                 print(tweet.__dict__.keys())
                #print(tweet)
                
                urls = ''
                hashtags = ''
                if tweet.urls:
                    for u in tweet.urls:
                        urls += u.expanded_url + ' '
#                     print(urls)
                if tweet.hashtags:
                    for h in tweet.hashtags:
                        hashtags += '#'+ h.text + ' '
#                     print(hashtags)
                cleanText = tweet.text.replace('\n',' ') # removendo quebras de linha
                cleanText = cleanText.replace(',','') # removendo o caracter de virgula, evitando quebrar o csv
                #print('{} {} {} {}'.format(tweet.created_at,cleanText,hashtags,urls))
            
                spamwriter.writerow([tweet.created_at,tweet.favorite_count,tweet.retweet_count,cleanText.encode('utf-8').decode('utf-8'),hashtags,urls])
                    
            print('processado {} tweets'.format(tweetCount))
        
            max_id = new_tweets[-1].id
        print("\n\n{} tweets baixados e armazenados no arquivo {}".format(tweetCount,fullfilename))
    #return allTweets


# In[117]:


if __name__ == "__main__":
    query = '"Deficiência" OR "Deficiência Mental" OR "Cego" OR "Cegueira" OR "Surdo" OR "Surdez" OR "Autismo" OR "Autista"'
    qtde = 20000
    twitter_search(query,qtde)

