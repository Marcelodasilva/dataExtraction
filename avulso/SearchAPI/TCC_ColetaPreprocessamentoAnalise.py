
# coding: utf-8

# # Identificação de tendências das pessoas que publicam em redes sociais: estudo de caso praia de alter do chão em Santarém Pará
# 
# <p> este trabalho coletará tweets publicados nas redondezas da vila de alter do chão, classificando entre gênero e raça e identificando os principais tópicos abordados para em cada grupo</p>

# <p> serão coletados a maior quantidade de tweets usando 5km de raio de alcance usando uma <a href="https://www.google.com.br/maps/place/2%C2%B030'26.6%22S+54%C2%B057'08.3%22W/@-2.5073936,-54.9545017,17z/data=!3m1!4b1!4m6!3m5!1s0x0:0x0!7e2!8m2!3d-2.5073991!4d-54.9523134?hl=pt-BR">coordenada</a> definida no centro da vila de alter do chão  </p>

# #### imports

# In[45]:


import pandas as pd
import twitter
import requests
import json
import re
import pprint
import numpy as np


# #### Credenciais

# In[14]:


TWITTER_API_KEY = 'ni1Iem3m6JMad29CB70tNIkhH'
TWITTER_API_SECRET = 'qsZ55TMxgpvPjmjOIu1iLGWWnct4CgUpHjvrJsm3NYEkMij9cS'
TWITTER_ACCESS_TOKEN = '178121845-hpSZn2MiHsghf8SQSUXlWrvibIP8vrq34Js8iQHQ'
TWITTER_ACCESS_TOKEN_SECRET = '6CFpYyYdwxTmLqWM8y4qdr6mVgUV6g28C4tYbQz0qkqpd'

FPP_KEY = 'fXZLbyUbnR-sk8B0DFkc7AZCuLvgxuvv'
FPP_SECRET = '3OqwElEu2RtmfsqWZdylujm6qqEQ8RL3'


# #### Função de autenticação api Twitter

# In[15]:


def authApi():
    """
    credenciais para autenticação
    """
    api = twitter.Api(consumer_key=TWITTER_API_KEY,
                      consumer_secret=TWITTER_API_SECRET,
                      access_token_key=TWITTER_ACCESS_TOKEN,
                      access_token_secret=TWITTER_ACCESS_TOKEN_SECRET)

    return api


# #### Função de Extração dos tweets

# In[16]:


#função de extração
def tweet_search(query, max_tweets):
    api = authApi()
    searchQuery = query  # termo buscado
    maxTweets = max_tweets # variável com maximo de tweets a serem obtidos
    tweetsPerQry = 100  # limite máximo de tweets por requisição
    
    allTweets = [] # lista final a ser retornada com todos os tweets
    geocode = ("-2.507389", "-54.952306", "5km") # ooordenada e raio de abrangência

    sinceId = None
    max_id = -1
    tweetCount = 0

    print("baixando no maximo {0} tweets".format(maxTweets))

    while tweetCount < maxTweets:
        if (max_id <= 0):
            if (not sinceId):
                new_tweets = api.GetSearch(term=searchQuery, count=tweetsPerQry)
            else:
                new_tweets = api.GetSearch(term=searchQuery, count=tweetsPerQry,
                                    since_id=sinceId)
        else:
            if (not sinceId):
                new_tweets = api.GetSearch(term=searchQuery, count=tweetsPerQry,
                                    max_id=str(max_id - 1))
            else:
                new_tweets = api.GetSearch(term=searchQuery, count=tweetsPerQry,
                                    max_id=str(max_id - 1),
                                    since_id=sinceId)
        if not new_tweets:
            print("sem mais tweets encontrados")
            break
        tweetCount += len(new_tweets)
        for tweet in new_tweets:
            allTweets.append(tweet)
        print("{0} tweets baixados".format(tweetCount))
        max_id = new_tweets[-1].id
            
    return allTweets


# #### Função de verificação do gênero e raça
# 
# A função recebe como parâmetro a listas de tweets obtidos, e faz uso da imagem de perfil para inferir o gênero e a raça do usuário 

# In[63]:


def getGE(tweets):
    listUser = []
    listName = []
    listStatus = []
    listGenero = []
    listRaca = []
    listSource = []
    
    pattern = '<a [^>]+>([^<]+)<\/a>' # padrão para reconhecimento da fonte de cada tweet
    ERRO_API = 'UNK_API' # clausula de erro quando houver erro por parte da api
    ERRO_IMG = 'UNK_IMG' # clausula de erro quando houver erro no reconhecimento da face na imagem
    
    for (i,t) in enumerate(tweets):
        src = re.search(pattern,t.source)
        img = t.user.profile_image_url
        img = re.sub('(_normal)', '', img) # removendo o sufixo _normal da url das imagens, sem este as imagens obtem maior resolução
        if(i % 5 == 0):
            print(f'primeiros {i} tweets')
		
		
        #parâmetros necessários para uso da api do Face++
        
        payload = {'api_key':FPP_KEY,
                   'api_secret':FPP_SECRET,
                   'image_url':img,
                   'return_attributes':'gender,ethnicity'
                  }
        
        r = requests.post('https://api-us.faceplusplus.com/facepp/v3/detect',params=payload)
        data = json.loads(r.text)
     
        if 'faces' in data and data['faces']: #checando se existe a chave e se ela não é vazia
#             print('Usuario {}\n Link imagem {}\n Genero {}\n Raça {}\n'.format(t.user.screen_name,img,data['faces'][0]['attributes']['gender']['value'],
#                             data['faces'][0]['attributes']['ethnicity']['value']))
            listUser.append(t.user.name)
            listName.append(t.user.screen_name)
            listStatus.append(t.text)
            listGenero.append(data['faces'][0]['attributes']['gender']['value'])
            listRaca.append(data['faces'][0]['attributes']['ethnicity']['value'])
            listSource.append(src[1])
            
            
            
        elif 'error_message' in data: #checando se existe a chave, caso exista, pressupoe que houve algum erro com a imagem ao ser enviada para api
#             print('Usuario {}\n Link imagem {}\n Genero {}\n Raça {} \n obs: problema na requisição da api -> Erro {}\n'.format(t.user.screen_name,img,'desconhecido','desconhecido',data['error_message']))
            
            listUser.append(t.user.name)
            listName.append(t.user.screen_name)
            listStatus.append(t.text)
            listGenero.append(ERRO_API)
            listRaca.append(ERRO_API)
            listSource.append(src[1])
            
        else: #caso caia nessa clausula, garante que a api processou a imagem com sucesso, todavia não foi possível identificar face nesta, retornando então a chave faces vazia
#             print('Usuario {}\n Link imagem {}\n Genero {}\n Raça {} \n obs: problema na imagem\n'.format(t.user.screen_name,img,'desconhecido','desconhecido'))
            listUser.append(t.user.name)
            listName.append(t.user.screen_name)
            listStatus.append(t.text)
            listGenero.append(ERRO_IMG)
            listRaca.append(ERRO_IMG)
            listSource.append(src[1])
            
    #criando o dataframe dos dados obtidos
    dif = {'usuario':listUser, 'nome':listName,'status':listStatus,'genero':listGenero,'etnia': listRaca,'fonte':listSource}
    df = pd.DataFrame(data=dif)
    
    return df



# In[72]:


#função Main
if __name__ == "__main__":
    query = input("Termo a ser buscado -> ")
#    query = "alter do chao"
    queryClean = re.sub('[!#]', 'Hashtag_', query) # removendo o simbolo de hashtag da string a ser usada para identificar o nome do arquivo
    filename = 'df_{}.csv'.format(queryClean)
    
    quantidade = int(input("Quantidade de tweets a serem buscados -> "))
#    quantidade = 1500
    tweets = tweet_search(query,quantidade) # efetuando a busca dos tweets
    #print(tweets)
    #print([t.text for t in tweets])
#     for t in tweets:
#         print(t.user.profile_image_url)
    df = getGE(tweets) # processa cada imagem de perfil dos usuarios de cada tweet obtido e identifica o sexo e o genero através da api do Face++
    print('aquisição e extração concluída\n armazenando em csv')
    df = df.replace('\n',' ', regex=True) #removendo a quebra de linha de cada instrução dentro do dataframe
    df.to_csv(filename) # armazenando o dataframe em arquivo csv
#    print(df.groupby(['etnia','genero']).size()) # exibe a contagem do numero de publicações que cada grupo etnia/genero fez dentro do dataset
#    print(df['fonte'].value_counts()) #exibe o quantitativo dos tweets por fonte de publicação (android, iphone, web, tweetdeck, instagram...)
    print('fim')


# In[110]:


#     print('fontes existentes')
#     df['fonte'].value_counts()
#     df['etnia'].value_counts()
	


# In[111]:





# In[ ]:


#from IPython.display import display, Markdown, Image

#     print(display_markdown('<a href="{0}">{1}</a>\n'.format(t.user.profile_image_url,t.user.name)))
   # display(Markdown('<a href="{0}">{1}</a>'.format(t.user.profile_image_url,t.user.screen_name)))

