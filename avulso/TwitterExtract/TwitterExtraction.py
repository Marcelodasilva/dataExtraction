import pprint
import csv
import twitter
import jsonpickle
import unicodedata
import json
import numpy as np
import requests
from TwitterExtract.config import *
import re

"""
authentication function

return twitter.Api instance

"""
def authApi():
    """
    credenciais para autenticação estão salvas no arquivo config.py
    """
    api = twitter.Api(consumer_key=TWITTER_API_KEY,
                      consumer_secret=TWITTER_API_SECRET,
                      access_token_key=TWITTER_ACCESS_TOKEN,
                      access_token_secret=TWITTER_ACCESS_TOKEN_SECRET)

    return api



"""
 search for tweets on twitter api

:param query 
    termo a ser buscado
:param max_tweets
    numero de tweets
"""

def tweet_search(query, max_tweets):

    api = authApi()
    #raw_query = requests.utils.quote('{} -filter:retweets & -filter:replies'.format(query))

    searchQuery = query  # this is what we're searching for
    maxTweets = max_tweets # Some arbitrary large number
    tweetsPerQry = 100  # this is the max the API permits
    queryFilterHashtag = re.sub('[!#]', 'Hashtag_', query)
    fName = 'data/tweets_{}_{}.csv'.format(queryFilterHashtag,max_tweets) # We'll store the tweets in a text file.

   # print(raw_query)


    sinceId = None
    max_id = -1

    tweetCount = 0
    print("baixando no maximo {0} tweets".format(maxTweets))
    with open(fName, 'w') as f:
        f.write("Usuário; Status\n")
        #f.write("Usuário; Status\n")
        while tweetCount < maxTweets:
                if (max_id <= 0):
                    if (not sinceId):
                        new_tweets = api.GetSearch(term=searchQuery, count=tweetsPerQry)
                    else:
                        new_tweets = api.GetSearch(term=searchQuery, count=tweetsPerQry,
                                            since_id=sinceId)
                else:
                    if (not sinceId):
                        new_tweets = api.GetSearch(term=searchQuery, count=tweetsPerQry,
                                            max_id=str(max_id - 1))
                    else:
                        new_tweets = api.GetSearch(term=searchQuery, count=tweetsPerQry,
                                            max_id=str(max_id - 1),
                                            since_id=sinceId)
                if not new_tweets:
                    print("sem mais tweets encontrados")
                    break
                for tweet in new_tweets:
                    f.write('{}{}{}{}\n'.format(unicodedata.normalize('NFKD', jsonpickle.encode(tweet.text)).encode('ascii','replace')))
                tweetCount += len(new_tweets)
                print("{0} tweets baixados".format(tweetCount))
                max_id = new_tweets[-1].id

    print ("baixados {0} tweets, salvo em {1}".format(tweetCount, fName))


if __name__ == "__main__":
    query = input("Termo a ser buscado -> ")

    quantidade = int(input("Quantidade de tweets a serem buscados -> "))

    tweet_search(query,quantidade)




