import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer 
from nltk.classify import NaiveBayesClassifier
from nltk.sentiment import SentimentAnalyzer
from nltk.sentiment.util import *
from nltk import tokenize
from nltk.corpus import stopwords
from string import punctuation
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
from sklearn.model_selection import cross_val_predict
import preprocessing
import pandas as pd
import numpy as np

stopwords = set(stopwords.words('portuguese') + list(punctuation))

hotel_rev = open('clean_hotels_review.txt','r',encoding='utf-8').readlines()
dataset_df = pd.read_csv('Tweets_Mg.csv',encoding='utf-8')
train_dataset = dataset_df["Text"].values
classes = dataset_df["Classificacao"].values
# print(classes[:5])
vectorizer = CountVectorizer(ngram_range = (2, 2))
f_tweets = vectorizer.fit_transform(train_dataset)
model = MultinomialNB()
model.fit(f_tweets, classes)

f_tests = vectorizer.transform(hotel_rev)
f_class = model.predict(f_tests)
from collections import Counter
print(Counter(model.predict(f_tests)))
df = pd.DataFrame(data={'text': hotel_rev, 'sentiment': f_class})
df.to_csv('sentiments_analysis.csv', sep=';', encoding='utf-8')






# results = cross_val_predict(model, f_tweets, classes, cv = 10)

# print(metrics.accuracy_score(classes, results))

# sentiments = ["Positivo", "Negativo", "Neutro"]
# print(metrics.classification_report(classes, results, sentiments))

# print(pd.crosstab(classes, results, rownames = ["Real"], colnames=["Predict"], margins=True))






# for h in hotel_rev:
#     palavras_tokenize = tokenize.word_tokenize(preprocessing.clean(h), language='portuguese')
#     palavras_sem_stopwords = [palavra for palavra in palavras_tokenize if palavra not in stopwords]









# sid = SentimentIntensityAnalyzer()
# for sentence in hotel_rev:
#     print(sentence)
#     ss = sid.polarity_scores(sentence)
#     for k in ss:
#         print('{0}: {1},  '.format(k, ss[k]),end='')
#     print('\n')









