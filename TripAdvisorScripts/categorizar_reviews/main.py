from pymongo import MongoClient
import re
import preprocessing



MONGO_HOST='mongodb://localhost/tripadvisordb'
client = MongoClient(MONGO_HOST)
mongodb = client.tripadvisordb
# total = len(list(mongodb.reviews_hotels.find({},{'localidade_autor':1,'_id':0})))
lista_informaram = mongodb.reviews_hotels.find({'regiao':'INEXISTENTE'}) 
total_informaram = len(list(lista_informaram))
# REGIOES = ['NORTE','NORDESTE','SUDESTE','CENTROESTE','SUL']
REGIOES = ['EXTERIOR']
for REG in REGIOES:
    with open(f'reviews_regiao_{REG}.txt','w',encoding='utf-8') as f:
        for r in mongodb.reviews_hotels.find({'regiao':REG}):
            f.write(preprocessing.clean(str(r['texto'])))











def checar_regiao(localidade):
    with open('estados_cidades.csv','r',encoding='utf-8') as f:
        linhas = f.readlines()
        NORTE = ['AC','AP','AM','PA','RO','RR','TO'] 
        NORDESTE = ['AL','BA','CE','MA','PB','PE','PI','RN','SE'] 
        CENTROESTE = ['GO','MT','MS','DF']
        SUDESTE = ['ES','MG','SP','RJ']
        SUL = ['PR','RS','SC']
        REGIAO = 'EXTERIOR'

        for l in linhas:
            l_list = l.split('|')
            if any(r in localidade for r in l_list):
                ESTADO = l_list[1]
                if ESTADO in NORTE:
                    REGIAO = 'NORTE'
                elif ESTADO in NORDESTE:
                    REGIAO = 'NORDESTE'
                elif ESTADO in CENTROESTE:
                    REGIAO = 'CENTROESTE'
                elif ESTADO in SUDESTE:
                    REGIAO = 'SUDESTE'
                elif ESTADO in SUL:
                    REGIAO = 'SUL'
                break
        return REGIAO
                        



    
    
    


        