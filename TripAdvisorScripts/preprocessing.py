import unicodedata
import re











def lower_case(text):
    return text.lower()

def remove_accentuation(text):
    text = unicodedata.normalize('NFKD', str(text)).encode('ASCII','ignore')
    return text.decode("utf-8")

def tokenize(text):
    # text.split() also works
    return text.split(" ")

def tokenize_refined(text):
    tokens = re.findall(r"[A-Z0-9-_]+|[@|#][\w\d_]+|!+|\.|,|[^\sA-Z0-9'!._]+", text, re.IGNORECASE)
    return tokens
def remove_punctuation(text):
    # re.sub(replace_expression, replace_string, target)
    #new_text = re.sub(r"\.|,|;|!|\?", "", text)
    new_text = re.sub(r"[^\w\d\s]+", " ", text)
    return new_text
def remove_numbers(text):
    # re.sub(replace_expression, replace_string, target)
    new_text = re.sub(r"[0-9]+", " ", text)
    return new_text


def clean(text):
    text = lower_case(text)
    text = remove_accentuation(text)
    text = remove_punctuation(text)
    text = remove_numbers(text)
    # text = tokenize_refined(text)
    return text
