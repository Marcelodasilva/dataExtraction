import re
import time
from pymongo import MongoClient
import datetime
import scrapy
from scrapy.selector import Selector
from bson.objectid import ObjectId
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import json
import sys

class Parser:
    def __init__(self):
        self.driver = webdriver.Firefox()
        self.TRIPADVISOR_BASEURL = 'https://www.tripadvisor.com.br'
        MONGO_HOST=  'mongodb://localhost/tripadvisordb'
        client = MongoClient(MONGO_HOST)
        self.mongodb = client.tripadvisordb_teste
        self.qtd_reviews = 0
    
    def check_exists_by_css_selector(self,css_selector):
        try:
            self.driver.find_element_by_css_selector(css_selector)
        except NoSuchElementException:
            return False
        return True




    def parse_hotels(self):
        # requisição de obtenção da pagina
        self.driver.get('https://www.tripadvisor.com.br/Hotels-g2442844-Alter_do_Chao_State_of_Para-Hotels.html')
        css_selector = 'span.current + a'
        count = 0
        sleep = 10
        while True:
            count +=1
            print('PAGINA ',count)
            print(f'DORMINDO {sleep} SEGUNDOS')
            time.sleep(sleep)
            print('INICIANDO - PARSEANDO LISTA DE HOTÉIS DA PAGINA ATUAL')
            self.parse_hotel_list(self.driver.page_source)
            # sel = Selector(text=self.driver.page_source)
            # print(sel.css('div#taplc_hsx_hotel_list_lite_dusty_hotels_combined_sponsored_0 div.listItem'))
            print('TERMINOU DE PARSEAR LISTA DE HOTEIS DA PAGINA ATUAL')
            if self.check_exists_by_css_selector(css_selector):
                # print('existe o elemento')
                element = self.driver.find_element_by_css_selector(css_selector)
                # print('vai clicar')
                element.click()
                print('CLICOU NO LINK QUE VAI PRA PROXIMA PAGINA\n\n')
                    
            else:
                print('FIM DAS PAGINAS - FIM')
                self.driver.close()
                break

    def parse_hotel_list(self,hotel_list_source):
        
        list_hotels_selector = Selector(text=hotel_list_source)
        list_hotels = list_hotels_selector.css('div[id^="taplc_hsx_hotel_list_lite_dusty_"]:not(div[id*="ab_hotels_sponsored"]) div.listItem')
        print(len(list_hotels))
        
        print('ABRINDO NOVA ABA')
        self.driver.execute_script("window.open('');")
        time.sleep(3)
        print('MUDANDO FOCO PARA A NOVA ABA')
        self.driver.switch_to.window(self.driver.window_handles[1])
        for l in list_hotels:
            print('VAI OBTER O LINK DO DO HOTEL')
            link_hotel = self.TRIPADVISOR_BASEURL+l.css('.property_title::attr(href)').extract_first()
            time.sleep(3)
            print('NAVEGANDO PARA A PAGINA DO HOTEL')
            self.driver.get(link_hotel)
            print('PAGINA CARREGADA - INDO PARA A FUNÇÃO DE EXTRAÇÃO DAS INFORMAÇÕES DO HOTEL')
            self.parse_hotel_info(self.driver.page_source)
            time.sleep(3)
        print('FECHANDO ABA CRIADA MAIS RECENTE')
        self.driver.close()
        time.sleep(3)
        print('MUDANDO FOCO PARA A ABA INICIAL')
        self.driver.switch_to.window(self.driver.window_handles[0])

    def parse_hotel_info(self,page_source):
        hotel_selector = Selector(text=page_source)
        print('EXTRAINDO INFORMAÇÕES DO HOTEL')
        nome_hotel = hotel_selector.css('h1#HEADING::text').extract_first()
        rating_hotel = hotel_selector.css('span.ui_bubble_rating::attr(alt)').extract_first()
        numero_avaliacoes_hotel = hotel_selector.css('span.reviewCount::text').extract_first()
        popularidade_hotel = f'{hotel_selector.css("span.popIndexValidation b.rank::text").extract_first()}{hotel_selector.css("span.popIndexValidation::text").extract_first()}{hotel_selector.css("span.popIndexValidation a::text").extract_first()}'
        ratingList_hotel = hotel_selector.css('ul.ratings_chart li.chart_row')
        avaliacao_hotel = {} # Excelente, Muito bom, Razoável, Ruim, Horrível
        
        for r in ratingList_hotel:
            avaliacao_hotel[r.css('span.row_label::text').extract_first()] = r.css('span.row_count::text').extract_first()
        
        servicos_hotel = hotel_selector.css('div.hrAmenitiesSectionWrapper div.textitem::text').extract()
        if hotel_selector.css('div.is-shown-at-desktop div.sub_content div.prw_common_star_rating div.ui_star_rating'):
            categoria_hotel = re.match('star_(.+)',hotel_selector.css('div.is-shown-at-desktop div.sub_content div.prw_common_star_rating div.ui_star_rating::attr(class)').extract_first().split(' ')[1])[1]
        else:
            categoria_hotel = 'Não informado'
        faixa_preco_hotel = hotel_selector.xpath("//div[contains(concat(' ', normalize-space(@class), ' '), ' section_content ')]/div[text()='Faixa de preço']").css('div + div div.textitem::text').extract_first()
        
        # print('\n',
        #     nome_hotel,'\n',
        #     rating_hotel,'\n',
        #     numero_avaliacoes_hotel,'\n',
        #     popularidade_hotel,'\n',
        #     avaliacao_hotel,'\n',
        #     servicos_hotel,'\n',
        #     categoria_hotel,'\n',
        #     faixa_preco_hotel,'\n'
        #     )
        hotel_id = ObjectId()
        self.mongodb.hotels_teste.insert({
            "_id":hotel_id,
            "nome":nome_hotel,
            "rating":rating_hotel,
            "numero_avaliacoes":numero_avaliacoes_hotel,
            "popularidade":popularidade_hotel,
            "avaliacao":json.dumps(avaliacao_hotel),
            "servicos":json.dumps(servicos_hotel),
            "categoria":categoria_hotel,
            "faixa_preco":faixa_preco_hotel
            })
        print(f'HOTEL "{nome_hotel}" SALVO NO BANCO')
        print('INICIO DO PARSER DOS REVIEWS DO HOTEL')
        
        self.parse_hotel_reviews(hotel_id)
        print('REVIEWS DO HOTEL PARSEADO')
        
    def parse_hotel_reviews(self,hotel_id):
        # simulando click para ativar todo os idiomas
        
        css_sel = 'div.prw_common_responsive_pagination div.ui_pagination > div.pageNumbers > a.current + a.taLnk'
        
        #iterando pelos reviews

        
        reviews_selector = Selector(text=self.driver.page_source)
        qtde_h = 0
        while True:
            
            try:
                # print('CHECANDO SE TODOS OS IDIOMAS ESTÁ MARCADO')
                if not reviews_selector.css('div#REVIEWS div.choices div.ui_radio:first-of-type > input::attr(checked)'):
                    print('NÃO MARCADO - NECESSÁRIO CLICK')
                    self.driver.find_element_by_css_selector('div#REVIEWS div.choices div.ui_radio:first-of-type').click()
                else:
                    print('MARCADO')
            except:
                print('ELEMENTO NÃO ENCONTRADO - HOTEL NÃO TEM REVIEW')
                return
            print('EXTRAINDO INFORMAÇÕES DOS REVIEWS DA PAGINA ATUAL')
            
            items = self.driver.find_elements_by_css_selector('p.partial_entry > span')
            for i in items:
                try:
                    i.click()
                except:
                    pass
            
            time.sleep(5)
            reviews_selector = Selector(text=self.driver.page_source)
    
                
            for i,rev in enumerate(reviews_selector.css('div.listContainer div.review-container')):
                qtde_h+=1
                voto_review = re.match('bubble_(.)',rev.css('span.ui_bubble_rating::attr(class)').extract_first().split(' ')[1])[1]
                titulo_review = rev.css('span.noQuotes::text').extract_first()
                localidade_autor_review = rev.css('div.info_text div.userLoc strong::text').extract_first() or 'Não Informado'
                texto_review = rev.css('p.partial_entry')[0].css('p::text').extract_first()
                print(texto_review)
                possui_resposta_review = False
                if len(rev.css('p.partial_entry')) > 1:
                    possui_resposta_review = True

                
                # print(voto_review,localidade_autor_review,titulo_review)
                self.mongodb.reviews_hotels_teste.insert({
                    'estabelecimento_id':hotel_id,
                    'voto': voto_review,
                    'localidade_autor':localidade_autor_review,
                    'possui_resposta':possui_resposta_review,
                    'titulo':titulo_review,
                    'texto':texto_review
                    })
                print(f' REVIEW SALVO NO BANCO E REFERENCIADO AO HOTEL DE ID {hotel_id}')
                # print(len(rev.css('p.partial_entry')))
                # print(
                #     voto_review,'\n',
                #     localidade_autor_review,'\n',
                #     titulo_review,'\n',
                #     texto_review,'\n\n'
                # )


        # SIMULAÇÃO DE CLICK DA PAGINAÇÃO
            time.sleep(5)
            try:
                elemento = self.driver.find_element_by_css_selector(css_sel)
                elemento.click()
                reviews_selector = Selector(text=self.driver.page_source)
                print('INDO PARA A PROXIMA PAGINA DE REVIEWS DO HOTEL EM QUESTÃO')
            except:
                print('FIM DAS PAGINAS DE REVIEW DO HOTEL EM QUESTÃO')
                break
        print('REVIEWS COLETADOS DO HOTEL ',qtde_h)
        self.qtd_reviews += qtde_h
        print('TOTAL DE REVIEWS COLETADOS ATÉ O MOMENTO ',self.qtd_reviews)




    def parse_restaurants(self):
        self.driver.get('https://www.tripadvisor.com.br/Restaurants-g2442844-Alter_do_Chao_State_of_Para.html')
        restaurants_list_response = Selector(text=self.driver.page_source)
        restaurants_list = restaurants_list_response.css('div#EATERY_SEARCH_RESULTS div[id^="eatery_"]')
        self.parse_restaurants_list(restaurants_list)
        print('PARSEOU TODOS OS RESTAURANTES')
        self.driver.close()
    
    def parse_restaurants_list(self,restaurants_list):
        for res in restaurants_list:
            link_restaurant = self.TRIPADVISOR_BASEURL + res.css('a.property_title::attr(href)').extract_first()
            self.driver.get(link_restaurant)
            self.parse_restaurant_info(self.driver.page_source)
            time.sleep(5)

    def parse_restaurant_info(self,page_source):
        restaurant_info_selector = Selector(text=page_source)
        nome_rest = restaurant_info_selector.css('h1#HEADING::text').extract_first()
        rating_rest = restaurant_info_selector.css('span.ui_bubble_rating::attr(alt)').extract_first()
        numero_avaliacoes_rest = restaurant_info_selector.css('span.header_rating > div.rating > a.more > span::text').extract_first() + restaurant_info_selector.css('span.header_rating > div.rating > a.more::text').extract_first()
        ratingList_rest = restaurant_info_selector.css('ul.ratings_chart li.chart_row')
        avaliacao_rest = {}
        for rat in ratingList_rest:
            avaliacao_rest[rat.css('span.row_label::text').extract_first()] = rat.css('span.row_count::text').extract_first()
        servicos_rest = restaurant_info_selector.css('div.cuisines div div.text::text').extract()
        popularidade_rest = f'{restaurant_info_selector.css("span.popIndexValidation b span::text").extract_first()}{restaurant_info_selector.css("span.popIndexValidation::text").extract_first()}{restaurant_info_selector.css("span.popIndexValidation a::text").extract_first()}'
        pontList_rest = restaurant_info_selector.css('div.questionRatings div.is-6')
        pontuacoes_rest = {}
        for p in pontList_rest:
            pontuacoes_rest[p.css('span.text::text').extract_first()] = re.match('bubble_(.+)',p.css('span.ui_bubble_rating::attr(class)').extract_first().split(' ')[1])[1]
        # print(nome_rest,rating_rest,numero_avaliacoes_rest,avaliacao_rest,servicos_rest,pontuacoes_rest)


        restaurant_id = ObjectId()
        self.mongodb.restaurants_teste.insert({
            "_id":restaurant_id,
            "nome":nome_rest,
            "rating":rating_rest,
            "numero_avaliacoes":numero_avaliacoes_rest,
            "popularidade":popularidade_rest,
            "avaliacao":json.dumps(avaliacao_rest),
            "servicos":json.dumps(servicos_rest)
            })



        self.parse_restaurants_reviews(restaurant_id)
    
    
    def parse_restaurants_reviews(self,restaurant_id):
        restaurants_reviews_selector = Selector(text=self.driver.page_source)
        print('restaurante id ',restaurant_id)
        while True:
            # clicado = False
            # try:
            #     print('CHECANDO SE A OPÇÃO TODOS OS IDIOMAS TÁ MARCADA')
            #     if not restaurants_reviews_selector.css('div.language li.filterItem:first-of-type::attr(checked)'):
            #         print('NÃO MARCADO - NECESSÁRIO CLICK')
            #         clicado = True
            #         self.driver.find_element_by_css_selector('div.language li.filterItem:first-of-type').click()
            #         restaurants_reviews_selector = Selector(text=self.driver.page_source)
            #         print('CLICADO')
            #     else:
            #         print('MARCADO')
            # except:
            #     if not clicado:
            #         print('ELEMENTO NÃO ENCONTRADO - RESTAURANTE NÃO TEM REVIEW')
            #         break

    
            for i,rev in enumerate(restaurants_reviews_selector.css('div[id^="review_"]')):
                voto_review = re.match('bubble_(.)',rev.css('span.ui_bubble_rating::attr(class)').extract_first().split(' ')[1])[1]
                titulo_review = rev.css('span.noQuotes::text').extract_first()
                possui_resposta_review = False
                if len(rev.css('p.partial_entry')) > 1:
                    possui_resposta_review = True
                texto_review = rev.css('p.partial_entry')[0].css('p::text').extract_first()
                
                localidade_autor_review = rev.css('div.member_info div.location span.userLocation::text').extract_first() or 'Não Informado'

                self.mongodb.reviews_res_teste.insert({
                    'estabelecimento_id':restaurant_id,
                    'voto': voto_review,
                    'localidade_autor':localidade_autor_review,
                    'possui_resposta':possui_resposta_review,
                    'titulo':titulo_review,
                    'texto':texto_review
                    })
                print('REVIEW DO RESTAURANTE ARMAZENADO COM SUCESSO')
            
            time.sleep(5)
            try:
                elemento = self.driver.find_element_by_css_selector('div.prw_common_north_star_pagination div.ui_pagination > div.pageNumbers > a.current + a.taLnk')
                elemento.click()
                restaurants_reviews_selector = Selector(text=self.driver.page_source)
                print('INDO PARA A PROXIMA PAGINA DE REVIEWS DO RESTAURANTE EM QUESTÃO')
            except:
                print('FIM DAS PAGINAS DE REVIEW DO RESTAURANTE EM QUESTÃO')
                break


            






# MAIN
p = Parser()
inicio = datetime.datetime.now().replace(microsecond=0)
p.parse_hotels()
# p.parse_restaurants()
fim = datetime.datetime.now().replace(microsecond=0)
diff = fim - inicio
print('FIM\n\nTempo de execução ',diff)
