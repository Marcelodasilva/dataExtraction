import pandas as pd
from langdetect import detect
import csv
arquivos = ["q1.csv", "q2.csv", "q3.csv"]
texto_pt = []
portugues = 0
outras = 0
texto = []

for j in arquivos:

    print("Analisando o arquivo: %s" %(j))
    dataset = pd.read_csv(j, sep=";", encoding="utf-8")

    data_samples = dataset.iloc[1:, 3]



    for i in data_samples:
        texto.append(i)



    for i in range(len(texto)):
        try:
            lang = detect(texto[i])
            if lang == "pt":
                texto_pt.append(texto[i])
        except:
            lang = "Error"
            print("Error on line: %s" % i)



saida = open("resultado.csv", "w")
for line in texto_pt:
    saida.write(line)
    saida.write("\n")

saida.close()
